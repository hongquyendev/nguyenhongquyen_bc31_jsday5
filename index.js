var chonkhuvuc = function (loaikhuvuc) {
  switch (loaikhuvuc) {
    case "A": {
      return 2;
    }
    case "B": {
      return 1;
    }
    case "C": {
      return 0.5;
    }
    case "X": {
      return 0;
    }
  }
};
var chondoituong = function (loaidoituong) {
  switch (loaidoituong) {
    case "1": {
      return 2.5;
    }
    case "2": {
      return 1.5;
    }
    case "3": {
      return 1;
    }
    case "0": {
      return 0;
    }
  }
};

document.getElementById("run").addEventListener("click", function () {
  var diemchuan = document.getElementById("diemchuan").value * 1;
  var diem1 = document.getElementById("diem1").value * 1;
  var diem2 = document.getElementById("diem2").value * 1;
  var diem3 = document.getElementById("diem3").value * 1;
  var khuvuc = document.getElementById("khuvuc").value;
  var doituong = document.getElementById("doituong").value;
  var diemkhuvuc = chonkhuvuc(khuvuc);

  var diemdoituong = chondoituong(doituong);

  var diemtong = diem1 + diem2 + diem3 + diemkhuvuc + diemdoituong;

  if (diemtong >= diemchuan) {
    document.getElementById(
      "ketqua"
    ).innerHTML = `<div>Tổng điểm: ${diemtong} <br /> Chúc mừng bạn đã trúng tuyển.</div>`;
  } else {
    document.getElementById(
      "ketqua"
    ).innerHTML = `<div>Tổng điểm: ${diemtong} <br /> Bạn đã rớt.</div>`;
  }
});

// Bài 2

document.getElementById("tinhtien").addEventListener("click", function () {
  var hoten = document.getElementById("hoten").value;
  var sokw = document.getElementById("sokw").value * 1;
  var gia50kwdau = 500;
  var gia50kwke = 650;
  var gia100kwke = 850;
  var gia150kwke = 1100;
  var giatren150kw = 1300;
  var sotienphaitra;

  if (sokw <= 50) {
    sotienphaitra = sokw * gia50kwdau;
  } else {
    if (sokw <= 100) {
      sotienphaitra = 50 * gia50kwdau + (sokw - 50) * gia50kwke;
    } else {
      if (sokw <= 200) {
        sotienphaitra =
          50 * gia50kwdau + 50 * gia50kwke + (sokw - 100) * gia100kwke;
      } else {
        if (sokw <= 350) {
          sotienphaitra =
            50 * gia50kwdau +
            50 * gia50kwke +
            100 * gia100kwke +
            (sokw - 200) * gia150kwke;
        } else {
          sotienphaitra =
            50 * gia50kwdau +
            50 * gia50kwke +
            100 * gia100kwke +
            150 * gia150kwke +
            (sokw - 350) * giatren150kw;
        }
      }
    }
  }

  sotienphaitra = sotienphaitra.toLocaleString("it-IT", {
    style: "currency",
    currency: "VND",
  });

  document.getElementById(
    "sotienphaitra"
  ).innerHTML = `<div>Họ và tên: ${hoten} - Tiền điện ${sotienphaitra} </div>`;
});
